import os
import requests
import time

api_host=os.getenv('API_HOST','localhost')
api_port=os.getenv('API_PORT','5000')

print('http://' + api_host + ':' + api_port)
while True:
    try:
        r = requests.get('http://' + api_host + ':' + api_port)
        print(r)
        time.sleep(2)
    except Exception as e:
        print(e)

